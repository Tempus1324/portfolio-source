# Public place to store all my project source files

Please note all projects depend on LWJGL and Slick-Util.
You can obtain LWJGL from: http://lwjgl.org/download.php
You can obtain Slick-Util from: https://bitbucket.org/kevglass/slick/src/96a4b840204c/trunk/Slick/lib/slick-util.jar?at=development

All my projects are compiled against version 2.8.5 of LWJGL using the libraries: lwjgl.jar, lwjgl_util.jar, jinput.jar
Also compiled against slick-util.jar
